<?php

/**
 * Stackdog settings form.
 */
function stackdog_settings_form() {
  $form = array();
  
  // Global kill switch.
  $form['stackdog_trace_enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Traces'),
    '#description' => t('This is a global kill swtich to enable/disable traces.'),
    '#default_value' => variable_get('stackdog_trace_enabled', 0),
    '#options' => array(
      1 => t('Enabled'),
      0 => t('Disabled'),
    ),
  );
  
  return system_settings_form($form);
}

?>