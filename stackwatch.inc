<?php

/**
 * Implementation of hook_form().
 * @package stackwatch
 */
function stackwatch_form(&$node) {
  $type = node_get_types('type', $node);
  if(!empty($node->stackwatch)) {
    $stack_watch = $node->stackwatch;
  }
  // Base node fields, meaning the title and body.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#required' => TRUE,
    '#default_value' => $node->title,
    '#maxlength' => 255,
    '#weight' => -5,
  );
  
  // Setup the node body field.
  if($type->has_body) {
    $form['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);
    $form['body_field']['body']['#description'] = t('Enter a description for this stack watch.');
    $form['body_field']['#weight'] = -4;
  }
  
  // Custom Fields for the SW.
  $form['stackwatch'] = array(
    '#type' => 'fieldset',
    '#title' => t('Stack Watch'),
    '#collapsible' => TRUE,
  );
  $form['stackwatch']['#tree'] = TRUE;
  
  // Status of this SW.
  $form['stackwatch']['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#default_value' => isset($stack_watch) ? $stack_watch['status'] : STACKWATCH_STATUS_INACTIVE,
    '#options' => array(
      STACKWATCH_STATUS_ACTIVE => t('Active'),
      STACKWATCH_STATUS_INACTIVE => t('In Active'),
    ),
  );
  if(isset($stack_watch) && $stack_watch['status'] >= STACKWATCH_STATUS_ERROR) {
    $form['stackwatch']['status']['#options'][STACKWATCH_STATUS_ERROR] = t('In Active DUE TO ERROR');
    drupal_set_message(t('Please note that this is an inactive stack watch due to errors. You may reactive it here.'));
  }
  
  // The Watchdog ID to watch.
  $form['stackwatch']['watchdog_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Watchdog ID'),
    '#description' => t('If you know the specific watchdog id you wish to trace; e.g. node, user, etc.'),
    '#default_value' => isset($stack_watch) ? $stack_watch['watchdog_id'] : '',
  );
  
  // Severity Levels to watch.
  $levels = watchdog_severity_levels();

  unset($levels[0]);
  $form['stackwatch']['severity_levels'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Severity'),
    '#description' => t('What severity level should we watch for?'),
    '#options' => watchdog_severity_levels(),
    '#default_value' => isset($stack_watch) ? $stack_watch['severity_levels'] : watchdog_severity_levels(),
  );
  
  // Pattern to match on ( if any ).
  $form['stackwatch']['pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Pattern'),
    '#description' => t('The pattern to match. Leave blank to match anything/ignore.'),
    '#default_value' => isset($stack_watch) ? $stack_watch['pattern'] : '',
  );
  // Pattern Type
  $form['stackwatch']['pattern_type'] = array(
    '#type' => 'select',
    '#description' => t('Pattern type to match. Either a reg-ex or a simple string to match. For a reg-ex expression, do NOT include the delimters, e.g. "!" or "/".'),
    '#default_value' => isset($stack_watch) ? $stack_watch['pattern_type'] : 'S',
    '#options' => array(
      'S' => 'Simple String Match',
      'R' => 'Regular Expression',
    ),
  );
  
  // Sample Size to capture.
  $form['stackwatch']['max_samples'] = array(
    '#type' => 'select',
    '#title' => t('Sample Size'),
    '#description' => t('Select the number of times this SW should be triggered.'),
    '#default_value' => isset($stack_watch) ? $stack_watch['max_samples'] : 2,
    '#options' => drupal_map_assoc(range(1,20)),
  );
  
  // TODO: Text area so that one can practice their patterns on a sample
  // text of probabley the watchdog message they are trying to target.
  
  return $form;
}

/** Implementation of hook_validate(). **/

/** Implementation of hook_insert(). **/
function stackwatch_insert($node) {
  $record = $node->stackwatch;
  $record['nid'] = $node->nid;
  $levels = array();
  foreach($node->stackwatch['severity_levels'] as $k => $v) {
    if(!empty($v)) {
      $levels[$v] = $v;
    }
  }
  if(empty($levels)) {
    $levels = drupal_map_assoc(array_keys(watchdog_severity_levels()));
  }
  $record['severity_levels'] = empty($levels) ? '' : serialize($levels);
  drupal_write_record('stackdog_stackwatch', $record);
  drupal_get_destination('node/' . $node->nid);
  _stackwatch_rebuild_index();
  //_stackwatch_update_index($node);
}

/** Implementation of hook_update(). **/
function stackwatch_update($node) {
  $record = $node->stackwatch;
  $record['nid'] = $node->nid;
  $levels = array();
  foreach($node->stackwatch['severity_levels'] as $k => $v) {
    if(!empty($v)) {
      $levels[$v] = $v;
    }
  }
  if(empty($levels)) {
    $levels = drupal_map_assoc(array_keys(watchdog_severity_levels()));
  }
  $record['severity_levels'] = empty($levels) ? '' : serialize($levels);
  drupal_write_record('stackdog_stackwatch', $record, 'nid');
  drupal_get_destination('node/' . $node->nid);
  _stackwatch_rebuild_index();
  //_stackwatch_update_index($node);
}

/** Implementation of hook_delete(). **/
function stackwatch_delete($node) {
  db_query("DELETE FROM {stackdog_stackwatch} WHERE nid = %d", $node->nid);
}

/** Implementation of hook_load(). **/
function stackwatch_load(&$node) {
  $rec = db_fetch_array(db_query("SELECT * FROM {stackdog_stackwatch} WHERE nid = %d", $node->nid));
  $rec['severity_levels'] = unserialize($rec['severity_levels']);
  return array('stackwatch' => $rec);
}

?>