<?php

/**
 * Overview page to view the traces for a stack watch.
 * @param $node The node object, which should be a stackwatch node.
 * @param $sdtid The stackdog trace id if we want to display the
 * trace page information.
 */
function stacktraces_page($node) {
  $page = '';
  $page .= '<h2>Stack Traces</h2>';
  if(!empty($sdtid)) {
    $page .= stacktraces_trace_page($sdtid);
    return $page;
  }
  // Fetch the traces for this stack watch.
  
  $header = array(
    'sdtid',
    'ip',
    'message',
  );
  $fields = array(
    'sdtid',
    'sdwid',
    'ip',
    'message',
  );
  $fields = implode(",", $fields);
  $sql = "SELECT $fields FROM {stackdog_trace} WHERE sdwid = %d";
  $sql .= tablesort_sql($header);
  $trs = db_query_range($sql,$node->stackwatch['sdwid'],0,10);
  while($trace = db_fetch_object($trs)) {
    $row = array();
    $row[] = l($trace->sdtid, 'node/' . $node->nid . '/traces/' . $trace->sdtid);
    $row[] = $trace->ip;
    $row[] = $trace->message;
    $row[] = $trace->sample_size;
    $row[] = $trace->max_samples;
    $rows[] = $row;
  }
  
  if(empty($rows)) {
    $page .= t('No stack traces have been made for this Stack Watch.');
  }
  else {
    $page .= theme('table', $header, $rows);
    $page .= theme('pager');
  }
  return $page;
}

/**
 * Display information about a trace.
 */
function stacktraces_trace_page($sdtid) {
  $page = '';
  $trace = db_fetch_object(db_query("SELECT * FROM {stackdog_trace} WHERE sdtid = %d", $sdtid));
  
  // Basic Information
  $row = $rows = $header = array();
  $header = array(
    t('Label'),
    t('Value'),
  );
  $rows = array(
    array(
      t('Date'),
      format_date($trace->timestamp),
    ),
    array(
      t('Watchdog ID'),
      $trace->watchdog_id,
    ),
    array(
      t('Location'),
      $trace->link,
    ),
    // URI
    array(
      t('URI'),
      $trace->require_uri,
    ),
    // Referer
    array(
      t('Referer'),
      $trace->referer,
    ),
    // Message
    array(
      t('Message'),
      $trace->message,
    ),
  );
  $page .= theme('table', $header, $rows, NULL, t('Basic Information'));
  
  // Use Krumo if availiable.
  if(module_exists('devel') && has_krumo()) {
      foreach (unserialize($trace->trace) as $call) {
        $nicetrace[$call['function']] = $call;
      }
      $page .= krumo_ob($nicetrace);
      // Environment
      $nicetrace = array();
      foreach (unserialize($trace->environment) as $call) {
        $nicetrace[$call['function']] = $call;
      }
      $page .= krumo_ob($nicetrace);      
  }
  else {
    // General place holder so that we can render arbitrary
    // form elements.
    $form = array();
  
    // The Stack Trace
    $form['stack'] = array(
      '#title' => t('Stack Trace'),
      '#type' => 'textarea',
      '#value' => print_r(unserialize($trace->trace), 1),
    );
    $page .= drupal_render($form);
    
    // Envifonrment
    $form = array();
    $form['trace'] = array(
      '#title' => t('Environment'),
      '#type' => 'textarea',
      '#value' => print_r(unserialize($trace->environment), 1),
    );
    $page .= drupal_render($form);
  }
  
  return $page;
}

?>